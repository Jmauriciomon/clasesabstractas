
package com.mycompany.mavenproject1;

public abstract class FiguraGeometrica {
    
    private String nombre;
    private double area;

public FiguraGeometrica(String nombre) {
        this.nombre = nombre;
}
    
    public abstract double area();
    public abstract double perimetro();
    public abstract double volumen();
    
    @Override
    public String toString ()
    {
        return "\n"+ "Volúmen: "+ volumen()+ "\n" + "Área: " + area()+ "\n" + "Perímetro: " + perimetro ();
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}    



package com.mycompany.mavenproject1;

public class Rectangulo extends FiguraGeometrica{
    
    private double base;
    private double altura;

    public Rectangulo(double base, double altura) {
        
        super ("Rectangulo");
        this.base = base;
        this.altura = altura;
    }

    @Override
    public double area(){
    return base*altura;
    }
    
    @Override
    public double perimetro(){
    return 2* (base + altura);
    }
    
    @Override
    public double volumen(){
    return  Math.pow(base, 2)*altura;
    }
    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
}


package com.mycompany.mavenproject1;

public class Circulo extends FiguraGeometrica{
    
    private double radio;
    private double altura;

    public Circulo(double radio, double altura) {
        
        super("Circulo");
        this.radio = radio;
        this.radio = altura;
    }
        
    @Override
    public double area()
    {
        return Math.PI*Math.pow(radio, 2);
    }
    
    @Override
    public double perimetro()
    {
        return (Math.PI*2)*radio;
    }
    
    @Override
    public double volumen()
    {
        return Math.PI*(radio*radio)*altura;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }
    
    public double getaltura (){
     
        return altura;
    }
    
    public void setaltura(double altura){
        
        this.altura = altura;
    }

    
}
